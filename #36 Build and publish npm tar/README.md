# Exercise 4: Build and Publish npm Tar Package

To verify that the user created for Project 1 has the correct access to the npm hosted repository in Nexus, you'll build a Node.js application from the "Cloud & IaaS Basics exercises" into a tar package and publish it to your Nexus npm repository.

## Steps to Build and Publish Node.js Tar Package

### Step 1: Prepare Your Node.js Application

1. **Ensure your Node.js application is ready for publishing:**
   - Navigate to the root directory of your Node.js application.
   - Verify that the `package.json` file contains accurate metadata, including `name`, `version`, and any other relevant information.

### Step 2: Build Your Node.js Application

2. **Package your application:**
   - Use the `npm pack` command in the root directory of your project to create a tarball (`*.tgz`) of your application. This step simulates the building of your application if not already done:

    ```bash
    npm pack
    ```

   - This command creates a `.tgz` file for your project, which can be found in the same directory.

### Step 3: Publish the Tar Package to Nexus

3. **Publish your package to the Nexus npm repository:**
   - Use the `npm publish` command to publish the tar package to your configured npm repository in Nexus. Replace `{npm-repo-url-in-nexus}` with the actual URL of your npm hosted repository in Nexus and `{package-name}` with the path to the tarball file you generated:

    ```bash
    npm publish --registry={npm-repo-url-in-nexus} {package-name}.tgz
    ```

   - Ensure you're logged in to npm with the credentials provided to Project 1's team if required.

### Step 4: Verify the Package in Nexus

4. **Check the Nexus Repository Manager:**
   - Log in to your Nexus Repository Manager.
   - Navigate to your npm hosted repository to verify that the package has been successfully published.

## Best Practices

- **Access Control:** Ensure that the npm repository in Nexus is properly secured and that only authorized users can publish packages.
- **Semantic Versioning:** Adhere to [semantic versioning](https://semver.org/) practices when updating your Node.js application to ensure compatibility and clarity in package management.
- **Audit Your Dependencies:** Regularly audit your npm dependencies for vulnerabilities and keep them up to date.

# Contributions

Contributions to this guide are welcome. Please follow good coding practices and provide a detailed description of your changes or improvements.

# License

[LICENSE.md](https://gitlab.com/TayoLusi19/artifact-repository-manager-with-nexus/-/blob/main/LICENSE.md?ref_type=heads)

# Author

Adetayo Michael Ibijemilusi
