# Exercise 9: Automate Fetching and Starting the Node.js Application

To streamline the deployment process, you can automate the steps to fetch the latest version of a Node.js application from the Nexus npm repository, untar it, and run it on your server. This script simplifies repetitive tasks, ensuring that your server always runs the latest version of the application.

## Script for Automated Fetching and Running of the Application

### Requirements

- `jq`: A lightweight and flexible command-line JSON processor.
- `wget`: A free utility for non-interactive download of files from the web.
- `curl`: A tool to transfer data from or to a server.

### Steps to Create the Script

1. **Create a Bash Script:**
   - Start by creating a new bash script file, for example, `update-and-run.sh`.

    ```bash
    touch update-and-run.sh
    chmod +x update-and-run.sh
    ```

2. **Open the Script for Editing:**
   - Use a text editor to add the following content to your script.

    ```bash
    #!/bin/bash

    # Nexus Repository and User Credentials
    USER="{user}"
    PASSWORD="{password}"
    NEXUS_IP="{nexus-ip}"
    NODE_REPO="{node-repo}"

    # Fetch the latest artifact details and save them in a json file
    curl -u $USER:$PASSWORD -X GET "http://$NEXUS_IP:8081/service/rest/v1/components?repository=$NODE_REPO&sort=version" | jq "." > artifact.json

    # Extract the download URL from the artifact details
    artifactDownloadUrl=$(jq '.items[].assets[].downloadUrl' artifact.json --raw-output)

    # Download the artifact using the extracted URL
    wget --http-user=$USER --http-password=$PASSWORD $artifactDownloadUrl -O app-package.tgz

    # Untar the downloaded package
    tar -zxvf app-package.tgz

    # Assuming the application directory is named 'package'
    cd package || exit

    # Install dependencies and run the application
    npm install
    node server.js
    ```

3. **Execute the Script on the Droplet:**
   - Upload the script to your Digital Ocean droplet and execute it.

    ```bash
    ./update-and-run.sh
    ```

### Best Practices

- **Secure Credential Storage:** Avoid hardcoding credentials within the script. Consider using environment variables or secure secret management tools.
- **Error Handling:** Add error handling in your script to manage cases where the fetch or download fails.
- **Logging:** Implement logging within the script to capture the process's output for debugging purposes.

# Contributions

Contributions to this guide are welcome. Please follow good coding practices and provide a detailed description of your changes or improvements.

# License

[LICENSE.md](https://gitlab.com/TayoLusi19/artifact-repository-manager-with-nexus/-/blob/main/LICENSE.md?ref_type=heads)

# Author

Adetayo Michael Ibijemilusi
