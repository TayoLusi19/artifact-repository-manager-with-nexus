# Exercise 5: Create Maven Hosted Repository in Nexus

Creating a Maven hosted repository in Nexus Repository Manager allows you to store and manage your Java application artifacts securely. This step is crucial for artifact lifecycle management, including versioning, sharing, and deploying Java applications.

## Steps to Create a Maven Hosted Repository

### Step 1: Log in to Nexus Repository Manager

1. **Access the Nexus web interface** by navigating to `http://your_nexus_server:8081`.
2. **Log in** with your admin credentials to access the administrative features.

### Step 2: Navigate to Repository Configuration

1. **Go to the Administration section** in the Nexus sidebar menu.
2. **Select "Repositories"** under the "Repository" category to view the repository configurations.

### Step 3: Create a New Maven Hosted Repository

1. **Click on the "Create repository" button**, and select "maven2 (hosted)" from the list of repository types.
2. **Configure the Repository:**
    - **Name:** Enter a unique name for your repository, such as `maven-java-project`.
    - **Version policy:** Choose whether this repository will store release versions, snapshot versions, or both.
    - **Layout policy:** Select the layout policy that best fits your project (strict or permissive).
    - **Blob store:** Select the blob store where the repository data will be stored. You can use the default blob store or create a new one if needed for organizational purposes.
3. **Advanced Settings (optional):** Configure any advanced settings as necessary, such as deployment policy and cleanup policies.
4. **Save the repository** by clicking the "Create repository" button at the end of the form.

### Step 4: Use Your Maven Hosted Repository

- With your Maven hosted repository created, you can now configure your Maven `settings.xml` or your project's `pom.xml` to deploy artifacts to this repository.
- Ensure to configure the `<distributionManagement>` section in your `pom.xml` or provide the repository URL in your build tools to point to your newly created Maven hosted repository for artifact deployment.

## Best Practices

- **Access Control:** Set up proper permissions for your Maven hosted repository to ensure that only authorized users can deploy and manage artifacts.
- **Naming Conventions:** Use clear and consistent naming conventions for your repositories to make them easily identifiable and manageable.
- **Backup Your Repositories:** Regularly back up your Nexus blob stores to prevent data loss and ensure that your artifacts are recoverable in case of system failures.

# Contributions

Contributions to this guide are welcome. Please follow good coding practices and provide a detailed description of your changes or improvements.

# License

[LICENSE.md](https://gitlab.com/TayoLusi19/artifact-repository-manager-with-nexus/-/blob/main/LICENSE.md?ref_type=heads)

# Author

Adetayo Michael Ibijemilusi
