# Exercise 6: Create User for Team 2 in Nexus

To facilitate secure and efficient access to the Maven repository for Project 2's team, setting up dedicated user accounts in Nexus Repository Manager is essential. This exercise details the steps to create a new user for Team 2, granting them the necessary permissions to access the Maven repository.

## Steps to Create a User for Team 2

### Step 1: Log in to Nexus Repository Manager

1. **Access the Nexus web interface** at `http://your_nexus_server:8081`.
2. **Log in** with your administrator credentials to access the control panel.

### Step 2: Navigate to Security Settings

1. **Go to the 'Security' section** in the left sidebar of the Nexus Dashboard.
2. **Select 'Users'** to view and manage user accounts.

### Step 3: Create a New User

1. **Click on the 'Create local user' button** to start the process of adding a new user account.
2. **Fill in the User Details:**
    - **UserID:** Choose a unique identifier for the new user, such as `team2-user`.
    - **First Name & Last Name:** Enter the user's real name for easier identification.
    - **Email:** Provide a valid email address for the user.
    - **Status:** Make sure the status is set to 'Active' to enable immediate access.
    - **Password:** Assign a secure password for the new account.
3. **Assign Roles:** Grant the user roles that include permissions for accessing the Maven repository. You may need to predefine a role with the necessary repository privileges if it doesn't already exist.
4. **Save the new user** by clicking the 'Create local user' button to finalize the account setup.

### Step 4: Inform Team 2

- **Communicate the Credentials:** Share the login details with Team 2 securely, ensuring they understand the scope of their access and the importance of maintaining password confidentiality.

## Best Practices

- **Use Role-Based Access Control (RBAC):** Leverage roles to manage user permissions efficiently. Assign users to roles that match their responsibilities within the project.
- **Monitor Activity:** Regularly review access logs and user activities to ensure compliance with security policies.
- **Educate Users:** Ensure that all team members are aware of security best practices, including secure password management and the proper handling of sensitive data.

# Contributions

Contributions to this guide are welcome. Please follow good coding practices and provide a detailed description of your changes or improvements.

# License

[LICENSE.md](https://gitlab.com/TayoLusi19/artifact-repository-manager-with-nexus/-/blob/main/LICENSE.md?ref_type=heads)

# Author

Adetayo Michael Ibijemilusi
