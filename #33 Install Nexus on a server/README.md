# Exercise 1: Install Nexus Repository Manager on AWS Ubuntu Server

Nexus Repository Manager (Nexus) serves as a pivotal tool for managing software artifacts necessary for development, deployment, and provisioning. This exercise provides a step-by-step guide to installing Nexus on an Ubuntu server hosted on Amazon Web Services (AWS).

## Prerequisites

- An AWS EC2 instance running Ubuntu (preferably the latest stable version).
- SSH access to your instance.
- At least 4GB of RAM for optimal Nexus operation.

## Installation Steps

### Step 1: Access Your EC2 Instance

1. **SSH into your EC2 instance:**

    Use the SSH command with your instance's public DNS/IP and the private key file (.pem) associated with your instance.

    ```bash
    ssh -i /path/to/your/private-key.pem ubuntu@your_instance_public_dns
    ```

### Step 2: Prepare Your System

2. **Update and upgrade your Ubuntu packages to ensure they are up to date:**

    ```bash
    sudo apt update && sudo apt upgrade -y
    ```

### Step 3: Install Java

3. **Install Java 11, as it is required by Nexus:**

    ```bash
    sudo apt install openjdk-11-jre-headless -y
    ```

### Step 4: Download and Install Nexus

4. **Download the latest version of Nexus Repository Manager OSS:**

    Navigate to the `/opt` directory and use `wget` to download Nexus. Check the [Sonatype website](https://www.sonatype.com/nexus-repository-oss) for the latest version URL.

    ```bash
    cd /opt
    sudo wget https://download.sonatype.com/nexus/3/latest-unix.tar.gz
    ```

5. **Extract the Nexus tarball and adjust ownership:**

    ```bash
    sudo tar -zxvf latest-unix.tar.gz
    sudo mv nexus-3* nexus
    sudo chown -R ubuntu:ubuntu nexus nexus-3*
    ```

### Step 5: Run Nexus

6. **Start Nexus:**

    Navigate to the Nexus bin directory and start Nexus using the included `nexus` script.

    ```bash
    cd nexus/bin
    ./nexus start
    ```

### Step 6: Access Nexus Web Interface

7. **Access the Nexus web interface:**

    Open a web browser and go to `http://your_instance_public_ip:8081`. The default admin credentials are provided during the first login attempt.

## Best Practices

- **Secure Your Nexus Instance:** Implement security groups in AWS to restrict access to the Nexus web interface only to known IPs for added security.
- **Regular Backups:** Schedule regular backups of your Nexus data to ensure you can recover your configurations and repositories in case of failure.
- **Monitor System Performance:** Keep an eye on your EC2 instance performance, especially if you're running Nexus alongside other applications.

# Contributions

Contributions to this guide are welcome. Please follow good coding practices and provide a detailed description of your changes or improvements.

# License

[LICENSE.md](https://gitlab.com/TayoLusi19/artifact-repository-manager-with-nexus/-/blob/main/LICENSE.md?ref_type=heads)

# Author

Adetayo Michael Ibijemilusi
