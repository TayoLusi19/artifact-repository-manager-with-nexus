# Exercise 3: Create User for Team 1 in Nexus

To ensure secure and controlled access to your npm hosted repository, it's essential to create specific users for different teams. This exercise guides you through creating a user in Nexus Repository Manager for Team 1, granting them access to the npm repository.

## Steps to Create a User for Team 1

### Step 1: Log in to Nexus Repository Manager

1. **Access the Nexus web interface** by navigating to `http://your_nexus_server:8081`.
2. **Log in** with your admin credentials.

### Step 2: Navigate to Security Settings

1. **Go to the Security** section in the administration menu on the left.
2. **Select "Users"** to view the current list of users.

### Step 3: Create a New User

1. **Click on the "Create local user" button** at the top right of the user list.
2. **Fill out the user details:**
    - **UserID:** Enter a unique identifier for the new user, such as `team1-user`.
    - **First name, Last name:** Enter the user's name.
    - **Email:** Provide the user's email address.
    - **Status:** Ensure the status is set to "Active" to allow the user access.
    - **Password:** Set a secure password for the account.
3. **Assign Roles:** Assign relevant roles to the user that grant access to the npm hosted repository. You might need to create or have already created a role that includes permissions for npm repositories.
4. **Click "Save"** to create the new user account.

### Step 4: Inform Team 1

- **Provide Credentials Securely:** Share the new user credentials with Team 1 securely, ensuring they understand the access scope and responsibilities.

## Best Practices

- **Use Role-Based Access Control (RBAC):** Define roles with specific permissions to manage access more efficiently and securely.
- **Regularly Review Access:** Periodically review user access rights and roles to ensure they align with current project needs and security policies.
- **Secure Password Practices:** Encourage users to use strong, unique passwords and consider implementing password policies.

# Contributions

Contributions to this guide are welcome. Please follow good coding practices and provide a detailed description of your changes or improvements.

# License

[LICENSE.md](https://gitlab.com/TayoLusi19/artifact-repository-manager-with-nexus/-/blob/main/LICENSE.md?ref_type=heads)

# Author

Adetayo Michael Ibijemilusi
