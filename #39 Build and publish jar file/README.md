# Exercise 7: Build and Publish JAR File to Maven Repository

To verify that the Team 2 user has proper access and to upload the first version of your Java application, you'll build a JAR file and publish it to your Nexus Maven repository. This exercise uses the `java-app` application from the Build Tools module.

## Steps to Build and Publish the JAR File

### Step 1: Prepare Your Java Application

1. **Navigate to your Java application directory:**
   - Ensure you're in the root directory of the `java-app` project where the `pom.xml` file is located.

### Step 2: Build the JAR File

2. **Use Maven to build the project:**
   - Run the following Maven command to compile your project and package it into a JAR file:
     ```bash
     mvn clean package
     ```
   - This command cleans the `target` directory, compiles your Java source code, and packages the compiled code into a JAR file within the `target` directory.

### Step 3: Configure Maven for Nexus Repository

3. **Configure your `settings.xml` for Maven:**
   - To publish the JAR file using the Team 2 user, you need to add the Nexus repository URL and the Team 2 user credentials to your Maven `settings.xml` file, typically located in the `.m2` directory.
   - Add the repository to the `<repositories>` and `<distributionManagement>` sections, and configure the Team 2 user credentials in the `<servers>` section of your `settings.xml`.

### Step 4: Publish the JAR File to Nexus

4. **Deploy the JAR file to the Nexus repository:**
   - With your Maven configuration set up, use the following command to deploy your JAR file to the Nexus Maven repository:
     ```bash
     mvn deploy
     ```
   - This command uploads the JAR file, along with any related metadata, to the specified Maven repository in Nexus using the credentials of the Team 2 user.

### Step 5: Verify the Upload in Nexus

5. **Check the Maven repository in Nexus:**
   - Log in to the Nexus Repository Manager web interface.
   - Navigate to the Maven repository where you deployed the JAR file to confirm that the upload was successful.

## Best Practices

- **Secure Credentials:** Ensure that the credentials for the Team 2 user are securely stored and not hardcoded in your `pom.xml` or `settings.xml`.
- **Versioning:** Use semantic versioning for your JAR file to facilitate clear and organized management of artifact versions.
- **Access Control:** Regularly review and manage access permissions for the Nexus Maven repository to ensure that only authorized users can deploy artifacts.

# Contributions

Contributions to this guide are welcome. Please follow good coding practices and provide a detailed description of your changes or improvements.

# License

[LICENSE.md](https://gitlab.com/TayoLusi19/artifact-repository-manager-with-nexus/-/blob/main/LICENSE.md?ref_type=heads)

# Author

Adetayo Michael Ibijemilusi
