# Exercise 8: Download from Nexus and Start Application on a Digital Ocean Droplet

This exercise demonstrates how to automate the process of fetching the latest version of a Node.js application from a Nexus repository and running it on a Digital Ocean droplet server. This involves creating a new server user with access to Nexus, using the Nexus REST API to find the download URL of the latest Node.js app artifact, downloading the artifact, and running the application.

## Steps to Fetch and Run the Latest Node.js Application

### Step 1: Create a New User for the Droplet Server

1. **Ensure the server user has the necessary permissions** to access both the npm and Maven repositories in Nexus. This might involve Nexus side configuration to grant the appropriate permissions to the user.

### Step 2: Use Nexus REST API to Fetch Download URL

2. **Fetch the download URL for the latest Node.js app artifact:**
   - Use the `curl` command with the Nexus REST API to fetch the latest artifact information. Replace `{user}`, `{password}`, `{nexus-ip}`, and `{node-repo}` with your Nexus user credentials, Nexus server IP, and the name of your npm repository, respectively.

    ```bash
    curl -u {user}:{password} -X GET 'http://{nexus-ip}:8081/service/rest/v1/components?repository={node-repo}&sort=version'
    ```

   - Parse the response to extract the download URL for the latest version of the Node.js app artifact.

### Step 3: Download the Latest Artifact

3. **Execute a command to download the artifact using the fetched URL:**
   - Once you have the direct download URL, use `curl` or `wget` with the appropriate authentication flags to download the artifact to your server.

    ```bash
    curl -u {user}:{password} -o app-package.tgz {download-url}
    ```

### Step 4: Untar and Run the Application

4. **Unpack the tarball and start the application:**
   - Untar the downloaded `.tgz` file to extract the Node.js application.

    ```bash
    tar -zxvf app-package.tgz
    ```

   - Change into the extracted directory (if necessary) and install any dependencies.

    ```bash
    npm install
    ```

   - Start the Node.js application.

    ```bash
    node server.js
    ```

## Best Practices

- **Securely Handle Credentials:** Ensure that the credentials used for accessing Nexus and performing downloads are securely managed, preferably using environment variables or secure vaults.
- **Automate Deployment Processes:** Consider automating the entire fetch and deploy process using scripts or CI/CD pipelines to minimize human error and streamline deployments.
- **Monitor Application Performance:** Once the application is running, use monitoring tools to keep an eye on its performance and health.

# Contributions

Contributions to this guide are welcome. Please follow good coding practices and provide a detailed description of your changes or improvements.

# License

[LICENSE.md](https://gitlab.com/TayoLusi19/artifact-repository-manager-with-nexus/-/blob/main/LICENSE.md?ref_type=heads)

# Author

Adetayo Michael Ibijemilusi
