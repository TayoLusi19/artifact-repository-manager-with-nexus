# Exercise 2: Create npm Hosted Repository in Nexus

Managing npm packages efficiently is crucial for Node.js application development. Nexus Repository Manager allows you to host your private npm packages, making them easily accessible for your development team. This exercise guides you through creating an npm hosted repository in Nexus, including setting up a new blob store for it.

## Steps to Create an npm Hosted Repository

### Step 1: Log in to Nexus Repository Manager

1. **Access the Nexus web interface** by navigating to `http://your_nexus_server:8081`.
2. **Log in** with your admin credentials.

### Step 2: Create a New Blob Store

1. **Navigate to the Blob Stores** section under the "Server" administration menu on the left.
2. **Click on the "Create blob store" button** at the top right.
3. **Select "File"** as the Blob store type.
4. **Enter a name** for your new blob store, such as `npm-blob-store`.
5. **Configure the blob store** settings as needed, or leave the defaults.
6. **Click "Create blob store"** to finalize the creation.

### Step 3: Create an npm Hosted Repository

1. **Navigate to the Repositories** section under the "Server" administration menu.
2. **Click on the "Create repository" button** and select "npm (hosted)".
3. **Enter a repository name** that clearly identifies it as your npm hosted repository, such as `npm-private`.
4. **Select the blob store** you created earlier (`npm-blob-store`) from the "Blob store" dropdown.
5. **Configure additional repository settings** as needed, including version policy (release or snapshot) and layout policy.
6. **Click "Create repository"** to finalize the setup.

### Step 4: Access and Use Your npm Hosted Repository

- **Configure npm to use your new repository** by setting the registry in your `.npmrc` file or using the `npm config set registry` command.
- **Publish npm packages** to your hosted repository using `npm publish` command.

## Best Practices

- **Secure Your Repository:** Configure proper access controls to ensure only authorized users can publish or access your npm packages.
- **Backup Regularly:** Ensure your Nexus data, especially blob stores, are backed up regularly to prevent data loss.
- **Monitor Disk Usage:** Keep an eye on the disk space used by your blob stores to manage storage capacity effectively.

# Contributions

Contributions to this guide are welcome. Please follow good coding practices and provide a detailed description of your changes or improvements.

# License

[LICENSE.md](https://gitlab.com/TayoLusi19/artifact-repository-manager-with-nexus/-/blob/main/LICENSE.md?ref_type=heads)

# Author

Adetayo Michael Ibijemilusi
